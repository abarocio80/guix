;; This is an operating system configuration generated
;; by the graphical installer.

(use-modules (gnu)
             (gnu packages linux))
	     ;;(nongnu packages linux))
(use-service-modules
  cups
  desktop
  networking
  ssh
  xorg)

(operating-system
  ;; (kernel linux)
  ;; (firmware (list linux-firmware))
  (locale "en_US.utf8")
  (timezone "America/Monterrey")
  (keyboard-layout (keyboard-layout "us" "dvorak"))
  (host-name "trashcan")
  (users (cons* (user-account
                  (name "barocio")
                  (comment "Alejandro Barocio A.")
                  (group "users")
                  (home-directory "/home/barocio")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))
  (packages
    (append ;; emacs with native-comp
     (list (specification->package "emacs-native-comp")
           ;; amd-microcode
           (specification->package "amd-microcode")
           ;; intel-microcode
           (specification->package "intel-microcode")
           ;; rtl8821ce-linux-module for some wifi cards
           (specification->package "rtl8821ce-linux-module")
           ;; git
           (specification->package "git")
           ;; htop
           (specification->package "htop")
           ;; alacritty
           (specification->package "alacritty")
           ;; emacs-guix
           (specification->package "emacs-guix")
           (specification->package "emacs-exwm")
           (specification->package
            "emacs-desktop-environment")
           (specification->package "nss-certs"))
     %base-packages))
  (services
    (append
     (list (service gnome-desktop-service-type)
           (service openssh-service-type)
           (service cups-service-type)
           (set-xorg-configuration
            (xorg-configuration
             (keyboard-layout keyboard-layout))))
     %desktop-services))
  
  ;; a module for wifi
  (kernel-loadable-modules (list rtl8821ce-linux-module))
  
  (bootloader
    (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets (list "/boot/efi"))
      (keyboard-layout keyboard-layout)))
  (swap-devices
    (list (swap-space
            (target
              (uuid "b76a820a-fcaa-41c6-8e0b-b6bb90b21bc3")))))
  (file-systems
    (cons* (file-system
             (mount-point "/boot/efi")
             (device (uuid "D467-E256" 'fat16))
             (type "vfat"))
           (file-system
             (mount-point "/")
             (device
               (uuid "eef6aea5-54f6-4d15-a2ff-fdc43db4ebf9"
                     'ext4))
             (type "ext4"))
           %base-file-systems)))
;; libnotify
;; bspwm
;; sxhkd
;; xinit
;; xorg-server
